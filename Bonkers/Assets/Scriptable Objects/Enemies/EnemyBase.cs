using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyTest", menuName = "Enemies/Test/EnemyBase", order = 1)]
public class EnemyBase : ScriptableObject
{
    public string enemyName;
    public int enemyMovementSpeed;
    public int enemyDamage;
    public int enemyKnockbackDealt;
    public int enemyKnockbackTaken;
    public float attackCooldownTime;


} 
