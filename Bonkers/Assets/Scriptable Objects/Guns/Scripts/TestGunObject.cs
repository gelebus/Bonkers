using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Test Gun", menuName = "Weapons/Gun/Test gun")]
public class TestGunObject : DefaultGunObject
{
    private void Awake()
    {

        Type = GunType.Test;
    }
}
