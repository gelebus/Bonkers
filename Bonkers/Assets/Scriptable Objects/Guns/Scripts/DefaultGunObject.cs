using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "New Default Object", menuName = "Inventory System/Items/Material")]

public enum GunType
{
    Pistol,
    Melee,
    Test,
}

public class DefaultGunObject : ScriptableObject
{
    public int KnockbackStrength;
    public string Name;
    public GunType Type;
    [TextArea(15,20)]
    public string Description;
    public int RoundsPerMinute;
}
