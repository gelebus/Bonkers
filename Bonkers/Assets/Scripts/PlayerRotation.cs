using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotation : MonoBehaviour
{
    [SerializeField] private float sensX = 90f;
    [SerializeField] private float sensY = 90f;

    [SerializeField] Transform Camera;
    [SerializeField] Transform Orientation;
    [SerializeField] Transform PlayerModel;

    float mouseX;
    float mouseY;

    float multiplier = 0.01f;

    float xRotation;
    float yRotation;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        HandleInput();
        Rotate();
    }

    //rotates the camera and the player according to input
    private void Rotate()
    {
        Camera.transform.localRotation = Quaternion.Euler(xRotation, yRotation, 0);
        Orientation.transform.rotation = Quaternion.Euler(0, yRotation, 0);
        PlayerModel.transform.rotation = Quaternion.Euler(0, yRotation, 0);
    }

    private void HandleInput()
    {
        if (Player.instance.InvertedMouse)
        {
            mouseY = -Input.GetAxisRaw("Mouse Y");
        }
        else
        {
            mouseY = Input.GetAxisRaw("Mouse Y");
        }
        mouseX = Input.GetAxisRaw("Mouse X");

        xRotation -= mouseY * sensY * multiplier;
        yRotation += mouseX * sensX * multiplier;

        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
    }
}
