using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    #region SingleTon
    public static InventoryManager instance;
    private void Awake()
    {
        if(instance == null || instance != this)
        {
            instance = this;
        }
    }
    #endregion

    [SerializeField] DefaultGunObject HeldGun;

    public DefaultGunObject GetHeldGun()
    {
        return HeldGun;
    }
}
