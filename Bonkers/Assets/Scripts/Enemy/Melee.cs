using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee : MonoBehaviour
{
    public GameObject player;
    public GameObject enemyMeleeCollider;
    public EnemyBase enemy;
    private float cooldownTime;

    public void Start()
    {
        cooldownTime = enemy.attackCooldownTime;
        player = Player.instance.transform.GetChild(0).gameObject;
    }

    public void Update()
    {
        if(cooldownTime > 0)
        {
            cooldownTime -= Time.deltaTime;
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (cooldownTime < 0)
        {
            if (other.gameObject.tag == "Player") 
            {
                Rigidbody playerRigidBody = other.GetComponentInParent<Rigidbody>();
                playerRigidBody.AddForce(transform.up * 10, ForceMode.Impulse);
                playerRigidBody.AddForce(transform.forward * enemy.enemyKnockbackDealt, ForceMode.Impulse);
                cooldownTime = enemy.attackCooldownTime;
                PlayerHud.Instance.PlayHitAnimation();
            }
        }
        
    }


}
