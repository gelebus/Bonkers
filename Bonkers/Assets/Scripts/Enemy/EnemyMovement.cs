using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{
    public EnemyBase enemy;
    public GameObject player;
    public NavMeshAgent agent;
    public Rigidbody rb;
    public int navAgentCooldown = 0;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(enemy.enemyName + ", " + enemy.enemyKnockbackDealt);
        player = Player.instance.gameObject.transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (agent.enabled)
        {
            Vector3 playerPos = player.transform.position;
            agent.speed = enemy.enemyMovementSpeed;
            agent.SetDestination(playerPos);
        }
    }
    private void FixedUpdate()
    {
        if(navAgentCooldown <= 0)
        {
            agent.enabled = true;
            rb.isKinematic = true;
            rb.useGravity = false;
        } else
        {
            agent.enabled = false;
            rb.isKinematic = false;
            rb.useGravity = true;
        }
        navAgentCooldown--;
    }

    public void Hit(Vector3 direction, float knockbackForce)
    {
        navAgentCooldown = Mathf.RoundToInt(1f / Time.fixedDeltaTime);
        rb.isKinematic = false;
        rb.useGravity = true;
        rb.AddForce(direction * knockbackForce, ForceMode.Impulse);
    }
}
