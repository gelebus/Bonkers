using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverHud : MonoBehaviour
{
    [SerializeField] private GameObject gameOverHudObject;
    [SerializeField] private GameObject InvertMouseHudObject;
    [SerializeField] private GameObject CurrentRoundHudObject;

    public static GameOverHud Instance;

    private void Awake()
    {
        if (Instance == null || Instance != this)
        {
            Instance = this;
        }
    }

    public void ShowGameOver()
    {
        Crosshair.Instance.SetVisible(false);
        InvertMouseHudObject.SetActive(false);
        CurrentRoundHudObject.SetActive(false);
        gameOverHudObject.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

    }

    public void TryAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
}
