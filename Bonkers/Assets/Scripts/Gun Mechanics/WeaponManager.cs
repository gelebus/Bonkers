using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class WeaponManager : MonoBehaviour
{
    [Header("Controls")]
    [SerializeField] private KeyCode primaryFire;

    [Header("Shoot effects")]
    [SerializeField] private GameObject muzzleEffectObject;
    [SerializeField] private GameObject objectImpactEffectObject;
    [SerializeField] private GameObject enemyImpactEffectObject;

    public LayerMask ignoreRaycast;

    private VisualEffect muzzleEffect;

    private int cooldown;
    private bool isFiring;
    private bool lastFirestate;

    private void Start()
    {
        muzzleEffect = muzzleEffectObject.GetComponentInChildren<VisualEffect>();
    }

    private void FixedUpdate()
    {
        isFiring = Input.GetButton("Fire1");

        if (isFiring && cooldown <= 0)
        {
            var heldGun = InventoryManager.instance.GetHeldGun();
            Shoot(heldGun);
            cooldown = Mathf.RoundToInt(60f / heldGun.RoundsPerMinute / Time.fixedDeltaTime);
        }
        lastFirestate = isFiring;
        cooldown--;
    }

    private void Shoot(DefaultGunObject heldGun)
    {
        Transform transform = Camera.main.transform;
        if (isFiring)
        {
            RaycastHit hit;
            if (!muzzleEffectObject.activeSelf)
            {
                muzzleEffectObject.SetActive(true);
            }
            muzzleEffect.Play();
            
            if (Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity, ~ignoreRaycast))
            {
                if (hit.collider.tag == "Enemy")
                {
                    Vector3 lookRotation = Quaternion.LookRotation(hit.normal).eulerAngles;
                    Quaternion rotation = Quaternion.Euler(new Vector3(lookRotation.x + 90, lookRotation.y, lookRotation.z));
                    GameObject obj = Instantiate(enemyImpactEffectObject, hit.point, rotation);
                    Destroy(obj, 5f);

                    hit.collider.GetComponent<EnemyMovement>().Hit(Camera.main.transform.forward, heldGun.KnockbackStrength);
                }
                else
                {
                    Vector3 lookRotation = Quaternion.LookRotation(hit.normal).eulerAngles;
                    Quaternion rotation = Quaternion.Euler(new Vector3(lookRotation.x + 90, lookRotation.y, lookRotation.z));
                    GameObject obj = Instantiate(objectImpactEffectObject, hit.point, rotation);
                    Destroy(obj, 5f);
                }
            }
        }
    }
}
