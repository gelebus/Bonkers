using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour
{
    [SerializeField] private Texture2D crosshairImage;
    [Range(0.1f, 2)]
    [SerializeField] private float crosshairScale = 0.5f;
    private bool crosshairVisible;

    public static Crosshair Instance;

    private void Awake()
    {
        if(Instance == null || Instance != this)
        {
            Instance = this;
        }
        crosshairVisible = true;
    }

    private void OnGUI()
    {
        if (crosshairVisible)
        {
            float xMin = (Screen.width / 2) - (crosshairImage.width * crosshairScale / 2);
            float yMin = (Screen.height / 2) - (crosshairImage.height * crosshairScale / 4);
            GUI.DrawTexture(new Rect(xMin, yMin, crosshairImage.width * crosshairScale, crosshairImage.height * crosshairScale), crosshairImage);
        }
    }
    
    public void SetVisible(bool visible)
    {
        crosshairVisible = visible;
    }
}
