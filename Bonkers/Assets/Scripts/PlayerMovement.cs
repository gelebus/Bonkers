using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] Transform Orientation;

    [Header("Movement")]
    public float MovementSpeed = 7f;
    [SerializeField] float MovementMultiplier = 10f;
    [SerializeField] float AirMultiplier = 0.3f;

    [Header("Drag")]
    public float groundDrag = 6f;
    public float airDrag = 2f;

    float HorMovement;
    float VerMovement;

    [Header("Jumping")]
    public float jumpForce = 10f;

    [Header("Keybinds")]
    [SerializeField] KeyCode jumpKey = KeyCode.Space;

    [Header("Ground Detection")]
    [SerializeField] LayerMask GroundLayer;
    bool IsGrounded;
    [SerializeField] float groundDistance = 0.25f;

    [Header("Player")]
    [SerializeField] float playerHeight = 1f;

    RaycastHit slopeHit;

    Vector3 MovementDirection;
    Vector3 SlopeMovementDirection;

    Rigidbody rb;

    private bool OnSlope()
    {
        if(Physics.Raycast(transform.position,Vector3.down, out slopeHit, playerHeight/2 + 0.8f))
        {
            if(slopeHit.normal != Vector3.up)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
    }

    private void Update()
    {
        IsGrounded = Physics.CheckSphere(transform.position - new Vector3(0,1,0), groundDistance, GroundLayer);

        HandleInput();
        HandleDrag();

        //If the player is on the ground and presses the jumpkey the player jumps
        if(Input.GetKeyDown(jumpKey) && IsGrounded)
        {
            Jump();
        }

        SlopeMovementDirection = Vector3.ProjectOnPlane(MovementDirection, slopeHit.normal);
    }

    private void Jump()
    {
        rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);
    }

    private void HandleDrag()
    {
        if (IsGrounded)
        {
            rb.drag = groundDrag;
        }
        else
        {
            rb.drag = airDrag;
        }
    }

    private void HandleInput()
    {
        HorMovement = Input.GetAxisRaw("Horizontal");
        VerMovement = Input.GetAxisRaw("Vertical");

        MovementDirection = Orientation.transform.forward * VerMovement + Orientation.transform.right * HorMovement;
    }
    
    private void FixedUpdate()
    {
        Move();
    }

    //Moves the player using rb.addforce (if grounded it uses regular speed else it uses the airmultiplier to alter the speed)
    private void Move()
    {
        if (IsGrounded && !OnSlope())
        {
            rb.useGravity = true;
            rb.AddForce(MovementDirection.normalized * MovementSpeed * MovementMultiplier, ForceMode.Acceleration);
        }
        else if(IsGrounded && OnSlope())
        {
            rb.useGravity = false;
            rb.AddForce(SlopeMovementDirection.normalized * MovementSpeed * MovementMultiplier, ForceMode.Acceleration);
        }
        else if(!IsGrounded)
        {
            rb.useGravity = true;
            rb.AddForce(MovementDirection.normalized * MovementSpeed * MovementMultiplier * AirMultiplier, ForceMode.Acceleration);
        }
        
    }
}
