using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public bool InvertedMouse;

    #region SingleTon
    public static Player instance;
    private void Awake()
    {
        if (instance == null || instance != this)
        {
            instance = this;
        }
    }
    #endregion

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            InvertedMouse = !InvertedMouse;
        }
    }
    public void Kill()
    {
        Debug.LogWarning("Player Died");
        GameOverHud.Instance.ShowGameOver();
    }
}
