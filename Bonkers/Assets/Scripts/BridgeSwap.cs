using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeSwap : MonoBehaviour
{
    private bool group1IsActive = true;
    [Header("Bridges")]
    [SerializeField] private GameObject BridgeGroup1;
    [SerializeField] private GameObject BridgeGroup2;
    [SerializeField] private GameObject BridgeGroup1Obstacle;
    [SerializeField] private GameObject BridgeGroup2Obstacle;
    [SerializeField] private GameObject BridgeGroup1Disable;
    [SerializeField] private GameObject BridgeGroup2Disable;

    [Header("Variables")]
    [SerializeField] private float cooldownLength;

    [Header("Materials")]
    [SerializeField] private Material active;
    [SerializeField] private Material inactive;

    private int cooldown;

    private void Start()
    {
        UpdateObjects();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && cooldown <= 0)
        {
            group1IsActive = !group1IsActive;
            cooldown = Mathf.RoundToInt(cooldownLength / Time.fixedDeltaTime);
            UpdateObjects();
            transform.parent.gameObject.GetComponent<Renderer>().material = inactive;
        }
    }

    private void UpdateObjects()
    {
        BridgeGroup1Disable.SetActive(!group1IsActive);
        BridgeGroup2Disable.SetActive(group1IsActive);
        Invoke("Switch", 0.1f);
    }

    private void Switch()
    {
        BridgeGroup1Obstacle.SetActive(!group1IsActive);
        BridgeGroup2Obstacle.SetActive(group1IsActive);
        BridgeGroup1.SetActive(group1IsActive);
        BridgeGroup2.SetActive(!group1IsActive);
    }

    private void FixedUpdate()
    {
        cooldown--;

        if(cooldown <= 0 && transform.parent.gameObject.GetComponent<Renderer>().material != active)
        {
            transform.parent.gameObject.GetComponent<Renderer>().material = active;
        }
    }
}
