using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.UI;

public class PlayerHud : MonoBehaviour
{
    [SerializeField] private float hitAnimationTime = 0.2f;
    [SerializeField] private TMP_Text waveText;

    public static PlayerHud Instance;

    private Vignette vignette;
    private int cooldown;

    private void Awake()
    {
        if (Instance == null || Instance != this)
        {
            Instance = this;
        }
        Camera.main.gameObject.GetComponent<Volume>().sharedProfile.TryGet<Vignette>(out vignette);
        vignette.intensity.Override(0f);
    }
    private void FixedUpdate()
    {
        if (cooldown <= 0)
        {
            vignette.intensity.Override(0f);
        }
        cooldown--;
    }

    public void PlayHitAnimation()
    {
        vignette.intensity.Override(0.3f);
        cooldown = Mathf.RoundToInt(hitAnimationTime / Time.fixedDeltaTime);
    }

    public void ChangeWave(int waveNumber)
    {
        waveText.text = $"Round: {waveNumber.ToString()}";
    }
}
