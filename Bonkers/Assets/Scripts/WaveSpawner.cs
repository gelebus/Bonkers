using UnityEngine;

public class WaveSpawner : MonoBehaviour
{
    public enum SpawnState { SPAWNING, WAITING, CHECKING };

    // The enemies to be spawned
    public GameObject[] enemies;    
    // The collection of spawn points
    public Transform spawnPointsParent;
    // The time between each wave
    public float cooldown = 1f;
    // The amount of enemies in the first wave
    public int startEnemyCount = 1;
    // The amount of enemies added for each wave
    public int enemyIncreasePerWave = 2;

    // The wave number
    private int waveNumber = 0;
    // The time left until the next wave
    private float waveCooldown;
    // The amount of enemies in the wave
    private int enemyCount;
    // The available spawn points
    private Transform[] spawnPoints;

    private SpawnState state = SpawnState.WAITING;


    void Start()
    {
        this.spawnPoints = InitializeSpawnPoints(spawnPointsParent);
        this.waveCooldown = this.cooldown;
        this.enemyCount = this.startEnemyCount;
    }

    void Update()
    {
        switch (this.state)
        {
            case SpawnState.WAITING:

                if (this.waveCooldown <= 0)
                {
                    // Spawn next wave
                    this.waveNumber++;
                    PlayerHud.Instance.ChangeWave(waveNumber);
                    this.SpawnWave();
                }
                else
                {
                    // Wait for cooldown to end
                    this.waveCooldown -= Time.deltaTime;
                }
                break;

            case SpawnState.CHECKING:

                // Check if all enemies are dead
                if (!AreEnemiesAlive())
                {
                    Debug.Log("All enemies cleared");
                    // Start cooldown before next wave
                    this.waveCooldown = this.cooldown;
                    this.state = SpawnState.WAITING;
                    Debug.Log("Cooldown for " + waveCooldown + " seconds");
                }
                break;
        }
    }


    private void SpawnWave()
    {
        this.state = SpawnState.SPAWNING;

        Debug.Log("Spawning wave " + this.waveNumber + ", with " + this.enemyCount + " enemies");

        // The amount of enemies per spawn point
        // (in case of more enemies than spawn points)
        int fullSpawns = this.enemyCount / this.spawnPoints.Length;
        // The amount of loose enemeis
        int exessEnemies = this.enemyCount % this.spawnPoints.Length;

        Vector3 offset;

        // Spawn enemies at each spawn point
        foreach (Transform sp in this.spawnPoints)
        {
            int xOffset = 0;
            int zOffset = 0;
            int increment = 2;
            bool changeX = true;
            for (int i = 0; i < fullSpawns; i++)
            {
                offset = new Vector3(xOffset, 0, zOffset);
                SpawnEnemy(this.enemies, sp, offset);

                if (Mathf.Abs(xOffset) == Mathf.Abs(zOffset))
                {
                    changeX = !changeX;

                    if (xOffset == zOffset)
                    {
                        if (xOffset <= 0)
                        {
                            xOffset -= 1;
                            zOffset += 1;
                            increment = Mathf.Abs(increment);
                            continue;
                        }
                        else
                        {
                            increment *= -1;
                        }
                    }
                }

                if (changeX)
                {
                    xOffset += increment;
                }
                else
                {
                    zOffset += increment;
                }

            }

            // Spawn 1 enemiy per spawn point unitl no more left to spawn
            if (exessEnemies > 0)
            {
                offset = new Vector3(xOffset, 0, zOffset);
                SpawnEnemy(this.enemies, sp, offset);
                exessEnemies--;
            }
        }

        this.state = SpawnState.CHECKING;

        this.enemyCount += this.enemyIncreasePerWave;
    }

    /// <summary>
    /// Spawns an enemy at a spawn point with an offset
    /// </summary>
    private static void SpawnEnemy(GameObject[] enemies, Transform spawnPoint, Vector3 offset)
    {
        GameObject enemy = enemies[Random.Range(0, enemies.Length)];
        Vector3 position = spawnPoint.position + offset;
        Instantiate(enemy, position, enemy.transform.rotation);
    }

    /// <summary>
    /// Extracts the children of a parent object containg the enemy spawnpoints
    /// </summary>
    /// <returns>An array with all enemy spawn points</returns>
    private static Transform[] InitializeSpawnPoints(Transform parent)
    {
        int spawnPointsCount = parent.childCount;
        if (spawnPointsCount <= 0)
        {
            Debug.LogError("No spawn points referenced.");
            return null;
        }
        else
        {
            Transform[] spawnPoints = new Transform[spawnPointsCount];
            for (int i = 0; i < spawnPointsCount; i++)
            {
                spawnPoints[i] = parent.GetChild(i);
            }
            return spawnPoints;
        }
    }

    /// <summary>
    /// Checks weather there are enemies still alive
    /// </summary>
    /// <returns>False when no enemies are alive</returns>
    private static bool AreEnemiesAlive()
    {
        if (GameObject.FindGameObjectWithTag("Enemy") == null)
        {
            return false;
        }
        return true;
    }
}
