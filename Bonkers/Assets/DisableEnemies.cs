using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableEnemies : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            other.gameObject.GetComponent<EnemyMovement>().navAgentCooldown = 5000000;
        }
    }
}
